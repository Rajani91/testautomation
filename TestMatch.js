describe("Validating Title",function(){

     beforeEach(function(){
        browser.get("http://www.way2automation.com/angularjs-protractor/calc/");
        title = browser.getTitle();
        date = '2019-12-12'

    });

    it("validate exact title", function(){

        title.then(function(text){
            console.log(text);
            expect(title).toEqual("Protractor practice website - Calculator");
            
        });
    });

   /* it("validate dates", function(){
       
            expect(date).toBeDate();
           // expect(date).toBeAfter(otherDate);
            //expect(date).toBeBefore(otherDate);
            expect(date).toBeValidDate();
        });*/
    

   it("validate title should not match", function(){

        title.then(function(text){
            console.log(text);
            expect(title).not.toEqual("Protractor practice website - Calculator");
        });
    });


    it("validate Partial title", function(){

        title.then(function(text){
            console.log(text);
            expect(title).toMatch("practice");
        });
    });
});