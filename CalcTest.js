

describe("Validating calc app",function(){

    var expectedTest;

    beforeEach(function(){
        browser.get("http://www.way2automation.com/angularjs-protractor/calc/");

        element(by.model("first")).sendKeys("10");
        element(by.model("second")).sendKeys("20");
        element(by.buttonText("Go!")).click();
        expectedTest=element(by.binding("latest")).getText();

    });

    afterEach(function(){
        console.log("after IT block");
    });

    it("validate add",function(){

        expectedTest.then(function(text){
            console.log("Result is: "+text);
            expect(parseInt(text)).toBe(30);
        });
   
    });

    it("validate add1",function(){

        expectedTest.then(function(text){

            console.log("Result is: "+text);
            expect(parseInt(text)).not.toBe(25);
     
        });

    });

    it("validate add1",function(){

        expectedTest.then(function(text){

            console.log("Result is: "+text);
            expect(parseInt(text)).toBe(24);
     
        });

    });
   
});